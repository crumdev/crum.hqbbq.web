# Headquarter BBQ Site

## Purpose  

Final project for my Code Louisville C#/Asp.Net development course created using the .Net Core 2.1 framework.

## Instructions for viewing

* Clone the repository to your computer
* Follow the instructions here -> [Get Started with .Net Core](https://www.microsoft.com/net/core) to install the .Net Core SDK and associated tools.  
* Once .Net Core SDK is installed open the command prompt and cd to the root of the repository that has crum.hqbbq.web.csproj in it.
* Run the commands below:
    * `> dotnet restore `  
    * `> dotnet run` **_// After this command you should see similar output as shown below_**  
     `
        Hosting environment: Development  
        Content root path: path\to\repository  
        Now listening on: http://localhost:5000  
        Application started. Press Ctrl+C to shut down.  
    `  
    * Navigate to the URL given after "Now listening on" which is typically `http://localhost:5000` and you should see the home page for the website.
