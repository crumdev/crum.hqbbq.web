﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace crum.hqbbq.web.ViewModels
{
    public class CreateMenuItemViewModel
    {
        public string Name { get; set; }
        public string Price { get; set; }
    }
}
