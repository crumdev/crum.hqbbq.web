using Microsoft.AspNetCore.Mvc;
using crum.hqbbq.web.ViewModels;
public class ContactController : Controller
{
    public IActionResult Index()
        {
            SendMsgViewModel msg = new SendMsgViewModel();
            return View(msg);
        }

    [HttpPost]
    public IActionResult SendMsg(SendMsgViewModel msg)
    {
        if(ModelState.IsValid)
        {
            @ViewBag.Feedback = "<div class=\"notification is-success\">Message Received!</div><br>";
        }
        else
        {
            @ViewBag.Feedback = "<div class=\"notification is-danger\">Error encountered during transmission. Please try again.</div><br>";
        }

        return View("Index");
    }
}