using Microsoft.AspNetCore.Mvc;
using crum.hqbbq.web.Data;
using crum.hqbbq.web.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using crum.hqbbq.web.ViewModels;

public class MenuController : Controller
{
    private readonly MenuContext _context;

    public MenuController(MenuContext context)
    {
        _context = context;
    }

    public async Task<IActionResult> Index()
    {
        return View(await _context.MenuItems.ToListAsync());
    }

    /*
     * 
     *  Add Methods for changing the value of an item in the database
     *  
     */
    public IActionResult Add()
    {
        var item = new CreateMenuItemViewModel();
        return View(item);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Add([Bind("ID,Name,Price")] MenuItem item)
    {
        if (ModelState.IsValid)
        {
            var newItem = item;
            try
            {
                _context.Add(item);
                await _context.SaveChangesAsync();
            } catch (DbUpdateException ex)
            {
                throw new InvalidOperationException(string.Format("The object could not be added. Make sure that a " + "product with a product number '{0}' does not aleady exist.\n", item.Name), ex);
            }

            return RedirectToAction("Index");
        }
        return View();
    }

    /*
     * 
     *  Edit Methods for changing the value of an item in the database
     *  
     */

    public async Task<IActionResult> Edit(int? id)
    {
        if(id == null)
        {
            return NotFound();
        }

        MenuItem item = await _context.MenuItems.SingleOrDefaultAsync(m => m.ID == id);

        if (item == null)
        {
            return NotFound();
        }

        return View(item);
        
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Price")] MenuItem item)
    {
        if (id != item.ID)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(item);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.MenuItems.Find(id) == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction("Index");
        }
        return View();
    }

    /*
     * 
     *  Delete Methods for changing the value of an item in the database
     *  
     */

    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var item = await _context.MenuItems
            .SingleOrDefaultAsync(m => m.ID == id);
        if (item == null)
        {
            return NotFound();
        }

        return View(item);
    }

    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var item = await _context.MenuItems.SingleOrDefaultAsync(m => m.ID == id);
        _context.MenuItems.Remove(item);
        await _context.SaveChangesAsync();
        return RedirectToAction("Index");
    }

    //Clean up of database connection
    private bool _disposed = false;

    protected override void Dispose(bool disposing)
    {
        if (_disposed)
            return;

        if(disposing)
        {
            _context.Dispose();
        }

        _disposed = true;

        base.Dispose(disposing);
    }
}