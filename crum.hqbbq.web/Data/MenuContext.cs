﻿using crum.hqbbq.web.Models;
using Microsoft.EntityFrameworkCore;

namespace crum.hqbbq.web.Data
{
    public class MenuContext : DbContext
    {
        public MenuContext(DbContextOptions<MenuContext> options) : base(options)
        {
        }

        public DbSet<MenuItem> MenuItems { get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MenuItem>().ToTable("MenuItem");
        }
    }
}
