﻿using crum.hqbbq.web.Models;
using System;
using System.Linq;

namespace crum.hqbbq.web.Data
{
    public static class DbInitializer
    {
        public static void Initialize(MenuContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            // Look for any Menu Items.
            if (context.MenuItems.Any())
            {
                return;   // DB has been seeded
            }

            var menuItems = new MenuItem[]
            {
                new MenuItem{Name="Rack of Ribs",Price=13.99m},
                new MenuItem{Name="Pulled Pork Sandwich",Price=7.99m},
                new MenuItem{Name="Dry Rub BBQ Chichken",Price=6.50m},
                new MenuItem{Name="Pulled Chicken Sandwich",Price=5.99m},
                new MenuItem{Name="House Salad",Price=3.99m},
                new MenuItem{Name="Coleslaw",Price=1.99m}
            };
            foreach (MenuItem m in menuItems)
            {
                context.MenuItems.Add(m);
            }
            context.SaveChanges();
        }
    }
}