using System.ComponentModel.DataAnnotations;

namespace crum.hqbbq.web.ViewModels
{
    public class SendMsgViewModel
    {
        public string Name {get; set;}
        public string Email {get; set;}
        public string Message {get; set;}
    }
}
