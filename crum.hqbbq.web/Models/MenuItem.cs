﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace crum.hqbbq.web.Models
{
    public class MenuItem
    {
        public int ID { get; set; }

        [Display(Name = "Item")]
        [StringLength(60,MinimumLength =3)]
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
    }
}
